var http = require('http')
var fs = require('fs');
var port = 8080;
var baseUrl = './';
http.createServer(function (request, response) {
//    console.log('request starting...');
	
	var filePath = baseUrl + request.url;
  	
	if (filePath === baseUrl + '/') {
	    response.writeHead(301,
	      {Location: baseUrl + 'public/demo.html'}
	    );
	    response.end();
	}
	//console.log("	request: " + filePath);
	fs.exists(filePath, function(exists) {

		if (exists) {
			fs.readFile(filePath, function(error, content) {
				if (error) {
					response.writeHead(500);
					response.end();
				}
				else {
					response.writeHead(200/*, { 'Content-Type': 'text/html' }*/);
					response.end(content, 'utf-8');
				}
			});
		}
		else {
			response.writeHead(404, {"Content-Type": "text/plain"});
			response.write("404 - Resurce Not found");
			response.end();
		}
	});
	
}).listen(port);
console.log('Server running on port ' + port);	//"at http://127.0.0.1:" + port